# coding=utf-8
# This is a sample Python script.

# Press ⌃R to execute it or replace it with your code.
# Press Double ⇧ to search everywhere for classes, files, tool windows, actions, and settings.

#Repositorio POO Parcial 2 por Irma Melissa Perez, 6-719-1376


class Cliente():

    def __init__(self, id, nombres, primerApellido, cedula, telefono, direccion):
        self.id = id
        self.nombres = nombres
        self.primerApellido = primerApellido
        self.cedula = cedula
        self.telefono = telefono
        self.direccion = direccion

    @property
    def __str__(self):
        return "nombres {}, {} primerApellido, {} cedula, {} telefono, {} direccion".format(self.nombres, self.primerApellido, self.cedula, self.telefono, self.direccion)


def insertar(cliente):

    return cliente.insert_one({
        "nombres": cliente.nombres,
        "primerApellido": cliente.primerApellido,
        "cantidad": cliente.cedula,
        "telefono": cliente.telefono,
        "direccion": cliente.direccion,
    }).inserted_id


def mostrar(clientes=None):
    return clientes.find()

def actualizar(id, clientes):
    resultado = clientes.update_one(
        {
            '_id': id(id)
        },
        {
            '$set': {
                "nombres": clientes.nombres,
                "primerApellido": clientes.primerApellido,
                "cedula": clientes.cedula,
                "telefono": clientes.telefono,
                "direccion": clientes.direccion
            }
        })
    return resultado.modified_count


def eliminar(id, clientes=None):
    resultado = clientes.delete_one(
        {
            '_id': Cliente(id)
        })
    return resultado.deleted_count




class Mascota (Cliente):

    def __init__(self, codigoCliente, nombres, apellido, cedula, telefono, direccion, codigoMascota, alias, especie, raza,
                 colorPelo, fechaNacimiento, pesoMedio, pesoActual):
            super().__init__(codigoMascota, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)  # utilizamos super()
            self.codigoMascota = codigoMascota
            self.alias = alias
            self.especie = especie
            self.raza = raza
            self.colorPelo = colorPelo
            self.fechaNacimiento = fechaNacimiento
            self.pesoMedio = float(input("Ingrese el Peso Medio de la Mascota:"))
            self.pesoActual = float(input("Ingrese el Peso Actual de la Mascota:"))

    def __str__(self):
            return super().__str__() + ", {} alias, {} especie, {} raza, {} colorPelo, {} fechaNacimiento, {} pesoMedio,{} pesoActual ".format(
            self.alias, self.especie, self.raza, self.colorPelo, self.fechaNacimiento, self.pesoMedio, self.pesoActual)

    def mostrar(mascotas=None):
        return mascotas.find()

    def actualizar(codigoMascota, mascotas):
        resultado = mascotas.update_one(
            {
                '_codigoMascota': codigoMascota(codigoMascota)
            },
            {
                '$set': {
                    "alias": mascotas.alias,
                    "especie": mascotas.especie,
                    "raza": mascotas.raza,
                    "colorPelo": mascotas.colorPelo,
                    "fechaNacimiento": mascotas.fechaNacimiento,
                    "pesoMedio": mascotas.pesoMedio,
                    "pesoActual": mascotas.pesoActual,
                }
            })
        return resultado.modified_count

    def eliminar(codigoMascota, mascotas=None):
        resultado = mascotas.delete_one(
            {
                '_id': Mascota(codigoMascota)
            })
        return resultado.deleted_count



class HistorialMedico (Mascota):

    def __init__(self, codigoMascota, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual, enfermedad, fechaEnfermo ):
            super().__init__(codigoMascota, alias, especie, raza, colorPelo, fechaNacimiento, pesoMedio, pesoActual)  # utilizamos super()
            self.enfermedad = enfermedad
            self.fechaEnfermo = fechaEnfermo

    def __str__(self):
            return super().__str__() + ", {} enfermedad, {} fechaEnfermo".format(
            self.enfermedad, self.fechaEnfermo)

    def mostrar(historialMedico=None):
        return historialMedico.find()

    def actualizar(codigoMascota, historialMedico):
        resultado = historialMedico.update_one(
            {
                '_codigoMascota': codigoMascota(codigoMascota)
            },
            {
                '$set': {
                    "enfermedad": historialMedico.enfermedad,
                    "fechaEnfermo": historialMedico.fechaEnfermo,
                }
            })
        return resultado.modified_count

    def eliminar(codigoMascota, historialMedico=None):
        resultado = historialMedico.delete_one(
            {
                '_codigoMascota': HistorialMedico(codigoMascota)
            })
        return resultado.deleted_count



class CalendariodeVacunacion (HistorialMedico):

    def __init__(self, enfermedad, fechaEnfermo, fechaCadaVacuna, enfermedadQueseVacuno ):
            super().__init__(enfermedad, fechaEnfermo)  # utilizamos super()
            self.fechaCadaVacuna = fechaCadaVacuna
            self.enfermedadQueseVacuno = enfermedadQueseVacuno

    def __str__(self):
            return super().__str__() + ", {} fechaCadaVacuna, {} enfermedadQueseVacuno".format(
            self.fechaCadaVacuna, self.enfermedadQueseVacuno)

    def mostrar(CalendariodeVacunacion=None):
        return CalendariodeVacunacion.find()

    def actualizar(id, CalendariodeVacunacion):
        resultado = CalendariodeVacunacion.update_one(
            {
                '_id': id(id)
            },
            {
                '$set': {
                    "fechaCadaVacuna": CalendariodeVacunacion.fechaCadaVacuna,
                    "enfermedadQueseVacuno": CalendariodeVacunacion.enfermedadQueseVacuno,
                }
            })
        return resultado.modified_count

    def eliminar(id, CalendariodeVacunacion=None):
        resultado = CalendariodeVacunacion.delete_one(
            {
                '_id': CalendariodeVacunacion(id)
            })
        return resultado.deleted_count







